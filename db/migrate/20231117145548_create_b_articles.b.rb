# This migration comes from b (originally 20231117095027)
class CreateBArticles < ActiveRecord::Migration[7.1]
  def change
    create_table :b_articles do |t|
      t.string :title
      t.text :text

      t.timestamps
    end
  end
end
