Rails.application.routes.draw do
  scope "/a" do
    resources :users
    get "up" => "rails/health#show", as: :rails_health_check
  end

  mount B::Engine => "/a", as: :b
end
